<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

    <meta name="description" content="Eurogroup es una organización que agrupa empresas de alta calidad y desempeño: Eurolicores C.A, Eurodistribution C.A y Euromobil C.A. Nuestra misión, ser un grupo lider en la distribución y comercialización de prestigiosas marcas siempre de la mano de nuestros aliados comerciales; para de esta manera contribuir en el desarrollo de nuestros clientes prestando asesoría profesional y oportuna.">
    <meta name="author" content="Eurogroup">

    <title>EuroGroup</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>

    <link href="css/general.css" rel="stylesheet">

    <link href="css/custom.css" rel="stylesheet">

    <!--link href="css/owl.carousel.css" rel="stylesheet"-->

    <!--link href="css/owl.theme.css" rel="stylesheet"-->
    <link rel="stylesheet" href="css/carucel.css">
    <link href="css/style.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">

    <link href="css/services.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <link rel="stylesheet" href="css/magnific-popup.css">

    <script src="js/modernizr-2.8.3.min.js"></script> 

    <link rel="icon" href=/img/estrella__eurogroup_512x512.png sizes="32x32" />
    <style>

        #myCarousel .carousel-caption {
            left:0;
            right:0;
            bottom:0;
            text-align:left;
            padding:10px;
            background:rgba(0,0,0,0.6);
            text-shadow:none;
        }

        #myCarousel .list-group {
            position:absolute;
            top:0;
            right:0;
        }
        #myCarousel .list-group-item {
            border-radius:0px;
            cursor:pointer;
        }
        #myCarousel .list-group .active {
            background-color:#eee;
        }

        @media (min-width: 992px) {
            #myCarousel {padding-right:33.3333%;}
            #myCarousel .carousel-controls {display:none;}
        }
        @media (max-width: 991px) {
            .carousel-caption p,
            #myCarousel .list-group {display:none;}
        }
    </style>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83247178-3', 'auto');
  ga('send', 'pageview');

</script>

<script src='https://www.google.com/recaptcha/api.js'></script>

</head>

