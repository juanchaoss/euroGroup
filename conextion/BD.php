<?php
/**
 * Created by PhpStorm.
 * User: Juanchaoss
 * Date: 15/8/2017
 * Time: 9:51 PM
 */

require_once "Conextion.php";

class BD{
    private $bd;

    public function __construct(){
        $this->bd = new Conextion();
    }

    public function getDataDB(){
        $stmt = $this->bd->getConection()->prepare("SELECT * FROM eg_posts WHERE post_status = 'publish' 
                                  ORDER BY post_date DESC LIMIT 5");
        $stmt->execute();
        $response = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->bd->closeConection();
        return $response;
    }
}