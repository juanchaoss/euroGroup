<?php
class Conextion{

    private $link;

    public function __construct(){
        try{
            $this->link =  new PDO("mysql:host=localhost;dbname=blog",'root','1234');
            $this->link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            return $e;
        }
    }

    public function getConection(){
        return $this->link;
    }

    public function closeConection(){
        return $this->link = null;
    }

}