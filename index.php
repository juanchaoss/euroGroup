

<?php
    include("head.php");
    include 'conextion/BD.php';
    $obj = new BD();
    $fetch = $obj->getDataDB();
    //var_dump($fetch[0]);
?>
<body id="home">

<!-- Preloader -->

<div id="preloader">

    <div id="status"></div>

</div>



<section class="service-w3l jarallax" id="service">

    <div class="overlay"></div>

    <div class="col-lg-12 col-md-12 col-sm-6 " data-aos="zoom-in" style="padding: 50px;">

        <div class="col-md-3 col-xs-4 col-centered">

            <img src="img/eurogroup.png" class="img-responsive logo">

        </div>

    </div>

    <div class="container">

        <div class="col-lg-4 col-md-4 col-sm-12 serv-agileinfo1" id="left">

            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree1" data-aos="zoom-in">
                <a href="http://localhost/wordpress/files/eurolicores" target="_blank">
                    <ul class="ch-grid">

                        <li>

                            <div class="ch-item">

                                <div class="ch-info">

                                    <div class="ch-info-front ch-img-1"></div>

                                    <div class="ch-info-back">

                                        <img src="img/licores.png" >

                                    </div>

                                </div>

                            </div>

                        </li>

                    </ul>

                    <h4 class="text-center" style="font-family:'Roboto', sans-serif">EuroLicores</h4>
                </a>
            </div>

            <div class="clearfix"></div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 serv-agileinfo2" id="middle">

            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree4" data-aos="zoom-in">
                <a href="http://localhost/wordpress/files/euromobil" target="_blank">
                    <ul class="ch-grid">

                        <li>

                            <div class="ch-item">

                                <div class="ch-info">

                                    <div class="ch-info-front ch-img-3"></div>

                                    <div class="ch-info-back">

                                        <img src="img/movil.png" >

                                    </div>

                                </div>

                            </div>


                        </li>

                    </ul>

                    <h4 class="text-center" style="font-family:'Roboto', sans-serif">EuroMobil</h4>
                </a>
            </div>

            <div class="clearfix"></div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 serv-agileinfo3" id="right">

            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree6" data-aos="zoom-in">
                <a href="http://localhost/wordpress/files/eurodistribution" target="_blank">
                    <ul class="ch-grid">

                        <li>

                            <div class="ch-item">

                                <div class="ch-info">

                                    <div class="ch-info-front ch-img-4"></div>

                                    <div class="ch-info-back">

                                        <img src="img/distribucion.png" >

                                    </div>

                                </div>

                            </div>

                        </li>

                    </ul>

                    <h4 class="text-center" style="font-family:'Roboto', sans-serif">EuroDistribution</h4>
                </a>


            </div>

            <div class="clearfix"></div>

        </div>

        <div class="text-center abcen wow fadeIn" style="bottom: 50px;right: 70px;">

            <div class="button_down ">

                <a class="imgcircle wow bounceInUp" data-wow-duration="1.5s"  href="#blog"> <img class="img_scroll" src="img/icon/circle.png" alt=""> </a>

            </div>

        </div>

        <div class="clearfix"></div>

    </div>

</section>

<!-- What is -->


<div  class="content-section-b" style="border-top: 0; background: url(img/bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 pull-right wow servicio fadeInRightBig">
                <div class="service-list">
                   
                    <div class="service-content">
                        <h4>Eficiencia</h4>
                        <p>Planificamos nuestros objetivos y trabajamos día a día para ofrecer 
                        calidad en nuestros servicios y alcanzar los resultados deseados.</p>
                    </div>
                    <div class="servicesimg ">
                        <img src="img/efi.png" >
                    </div>
                    
                    
                </div>
            </div>

            <div class="col-sm-6 pull-right wow servicio fadeInRightBig">
                
                <div class="service-content rigthtxt" >
                    <h4>Distribucion</h4>
                    <p>Los productos se encuentran a la disposición de nuestros compradores y son entregados a su destino en el tiempo solicitado.</p>
                </div>
                
                <img src="img/distri.png" class="servicesimg rigth">
            </div>
           
        </div>

        <div class="row">
            <div class="col-sm-6 pull-right wow servicio fadeInRightBig">
                <div class="service-list ">
                   
                    <div class="service-content ">
                        <h4>Crecimiento</h4>
                        <p>Innovamos estratégias y afrontamos nuevos retos que contribuyan a la estimulación del personal y de la organización.</p>
                    </div>
                    
                    <img src="img/creci.png" class="servicesimg ">
                    
                </div>
            </div>

            <div class="col-sm-6 pull-right wow servicio fadeInRightBig">
                
                <div class="service-content rigthtxt" >
                    <h4>Ventas</h4>
                    <p>Nos encargamos de informar acerca de nuestros productos y servicios con el fin de realizar ventas de gran volúmen con clientes dispuestos a mantener relaciones contractuales a largo plazo.</p>
                </div>
                
                <img src="img/venta.png" class="servicesimg rigth">
            </div>
           
        </div>
    </div>
</div>


<div id="blog" class="content-section-b" style="border-top: 0;padding: 90px 0 0;">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pull-right wow fadeInRightBig">
                <h3>Noticias</h3>

                <div class="">
                        <span class="carrusel"></span>
                        <div id="owl-carousel" class="owl-carousel principal">

                            <?php
                                foreach ($fetch as $fetchs){
                                   // var_dump($fetchs['ID']);

                                       echo '<div class="item">
                                            <figure class="box_owl">
                                                <img src="img/pampero1.jpg">
                                                <a href="'.$fetchs['guid'].'" class="title">'.$fetchs['post_title'].'</a>
                                        <div class="textonoti">
                                            <p> '.$fetchs['post_content'].'</p>
                                        </div>
            
                                        </figure>
                                    </div>';
                                }
                            ?>
                        </div>
                    </div>

             </div>
    </div>

</div>

<!--NEW SECTION-->

<div id="blog" class="content-section-b" style="border-top: 0">

    <div class="container">



        <div class="row">



            <div class="col-sm-6 pull-right wow fadeInRightBig">
                <img class="img-responsive " src="img/social.png" alt="">


        </div>
            <div class="col-sm-6 wow fadeInLeftBig"  data-animation-delay="200">

                <h3 class="section-heading">Responsabilidad Social</h3>

                <div class="sub-title lead3">Visita nuestro Blog</div>

                <p class="lead">

                    En eurogroup cumplimos con nuestra responsabilidad social, ayudando a asociaciones sin fines de lucro y aportando nuestro granito de arena para hacer de este mundo un lugar mejor.

                </p>

                <a class="btn btn-embossed btn-info" href="http://www.eurogroupvenezuela.com/blog" role="button">Visitar</a></p>

            </div>

        </div>

    </div>

</div>
<!-- Contact -->

<div id="contact" class="content-section-a" style="background: url(img/bg.png);">

    <div class="container">

        <div class="row">

            <div class="col-md-6 col-md-offset-3 text-center wrap_title">

                <h2 class=" wow fadeInUp" data-wow-delay="0.6s" >Contacto</h2>

            </div>

            <form   role="form" action="" method="post" >

                <div class="col-md-6">

                    <div class="form-group">

                        <label for="InputName" >Nombre</label>

                        <div class="input-group">

                            <input type="text" class="form-control" name="nombre" id="InputName" placeholder="Ingrese Nombre" required>

                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                        </div>

                    </div>

                    <div class="form-group">

                        <label for="InputEmail"  >Correo</label>

                        <div class="input-group">

                            <input type="email" class="form-control" id="InputEmail" name="correo" placeholder="Ingrese correo" required  >

                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                        </div>

                    </div>




                    <div class="form-group">
                        <label for="opc"  >¿Que quieres ser?</label>
                        <select name="opc" id="opc" class="form-control">

                            <option value="proveedor"  >Proveedor</option>

                            <option value="cliente" >Cliente</option>

                        </select>
                    </div>


                    <div class="form-group">
                        <label for="opc"  >Seleccione a quien desea enviar el mensaje:</label>
                        <select name="opcion" id="opcion" class="form-control" onchange="comprobar(value);">

                            <option value="EuroMobil" >EuroMobil</option>
                            <option value="EuroLicores"  >EuroLicores</option>
                            <option value="EuroDistribution" >EuroDistribution</option>

                        </select>
                    </div>



                    <div id="licencia" class="form-group" style="display:none">

                        <label  >Número de licencia de licores:</label>

                        <div class="input-group">

                            <input type="tetx" class="form-control" id="licencia" name="licencia" placeholder="Ingrese número de licencia"   >

                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                        </div>

                    </div>


                    <div id="direccion" class="form-group" style="display:none">

                        <label  >Dirección:</label>

                        <div class="input-group">

                            <input type="tetx" class="form-control" id="direccion" name="direccion" placeholder="Ingrese dirección"   >

                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                        </div>

                    </div>







                    <div class="form-group">

                        <label for="InputMessage"  >Mensaje</label>

                        <div class="input-group">

                            <textarea name="mensaje" id="InputMessage" class="form-control" rows="5" required></textarea>

                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                        </div>

                    </div>
                    <div class="g-recaptcha" data-sitekey="6LdrHB8UAAAAABPArZEo9oU4ygSD03hi_WwwbIHS" ></div>

                    <input type="submit" name="submit" id="submit" value="Enviar" class="btn wow tada btn-embossed btn-primary pull-right">

                </div>

            </form><?php

            if (isset($_REQUEST['submit'])) {


                $captcha=$_REQUEST['g-recaptcha-response'];

                if($captcha=="")
                {
                    echo "<SCRIPT>alert('Verifique Capcha')</SCRIPT>";

                }

                $secret = '6LdrHB8UAAAAAMJwWOznOeoGR6ptO-fda0bIcdET';

                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_REQUEST['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);

                if($responseData->success){

                    $tipo = $_REQUEST['opc'];

                    if ($tipo == "cliente") {



                        $para = 'kjimenez@eurogroup.com.ve';


                        $asunto = 'Datos Posible cliente';

                        $mensaje = "Nombre: " . $_REQUEST['nombre'] . "\r\n" . "Correo: " . $_REQUEST['correo'] . " \r\n" ."Destinado a: ".$_REQUEST['opcion'];


                        if($_REQUEST['opcion']=='EuroLicores')
                        {

                            $mensaje.= "\r\n" . "Número de licencia de licores: " . $_REQUEST['licencia'] . " \r\n" ."Dirección: ".$_REQUEST['direccion'];

                        }

                        $mensaje.= "\r\n"."Mensaje: " . $_REQUEST['mensaje'];


                        $de = "noreply@eurogroupvenezuela.com";

                        $headers = "From:$de";


                        if (mail($para, $asunto, $mensaje, $headers)) {

                            echo "<SCRIPT>alert('MENSAJE ENVIADO')</SCRIPT>";

                        }

                    }



                    else {


                        $para = 'kjimenez@eurogroup.com.ve';


                        $asunto = 'Datos Posible Proveedor';

                        $mensaje = "Nombre: " . $_REQUEST['nombre'] . "\r\n" . "Correo: " . $_REQUEST['correo'] . " \r\n" ."Destinado a: ".$_REQUEST['opcion'];


                        if($_REQUEST['opcion']=='EuroLicores')
                        {

                            $mensaje.= "\r\n" . "Número de licencia de licores: " . $_REQUEST['licencia'] . " \r\n" ."Dirección: ".$_REQUEST['direccion'];

                        }

                        $mensaje.= "\r\n"."Mensaje: " . $_REQUEST['mensaje'];



                        $de = "noreply@eurogroupvenezuela.com";

                        $headers = "From:$de";

                        if (mail($para, $asunto, $mensaje, $headers)) {

                            echo "<SCRIPT>alert('MENSAJE ENVIADO')</SCRIPT>";

                        }

                    }

                }
            }

            ?>

            <hr class="featurette-divider hidden-lg">

            <div class="col-md-5 col-md-push-1 address"><br>

                <h3 class=" wow fadeInUp" data-wow-delay="0.6s">Proveedores </h3>

                <p class="lead" class="wow fadeInUp" data-wow-delay="0.6s">

                    ¿Quieres formar parte de nuestros exclusivos proveedores?

                    ¿Quieres tú marca en todo el oriente del país?

                </p>
                <br><br>
                <h3 class=" wow fadeInUp" data-wow-delay="0.6s">Clientes</h3>

                <p class="lead" class="wow fadeInUp" data-wow-delay="0.6s">

                    ¿Quieres formar parte de nuestra cartera de clientes?

                    ¿Quieres ser atendido por Euro Group?

                </p>
                <br><br>
                <h3 class=" wow fadeInUp " data-wow-delay="0.6s">¿Quieres formar parte de nuestro equipo? <i class="fa fa-users" aria-hidden="true" style="font-weight:100;"></i>
                </h3>
                <br>





                <a href="registro.php" class="btn wow tada btn-embossed btn-primary " target="_blank"><span class="network-name" class=" wow fadeInUp" data-wow-delay="0.6s">Postúlate</span></a>

                <li class="social">

                    <a href="http://www.facebook.com/eurogroupca"><i class="fa fa-facebook-square fa-size"> </i></a>

                    <a href="https://www.instagram.com/eurogroup.ca/"><i class="fa  fa-instagram fa-size"> </i> </a>

                </li>

            </div>

        </div>

    </div>

</div>

<footer>

    <div class="container">

        <div class="row">

            <div class="col-md-7">

                <a class="footer-title">Create by </a>  <a class="footer-title" target="blank" href="http://oxoftmedia.com/">OXOFTMEDIA </a> <a class="footer-title"> and </a> <a class="footer-title" target="blank" href="http://vccodes.com.ve/"> VC CODES</a>

            </div> <!-- /col-xs-7 -->

        </div>

    </div>

</footer>
    <script src="js/jquery-1.10.2.js"></script>
<script type="text/javascript">
    function comprobar(value)
    {

        if(value==='EuroLicores')
        {

            $('#licencia').css('display','inline');
            $('#direccion').css('display','inline');

        }

        else
        {
            $('#licencia').css('display','none');
            $('#direccion').css('display','none');

        }

    }

</script>
<?php include("script.php"); ?>
    <script>
        $('#owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>
</body>



