<?php include("head.php"); ?><body id="home">



    <div id="contact" style= "padding: 20px 0 ">

        <div class="container">

            <div class="row" >



                <div class="col-md-6 col-md-offset-3 text-center wrap_title">

                    <h3 class=" wow fadeInUp" data-wow-delay="0.1s" style="font-family: 'Roboto', sans-serif" >¿Te gustan los retos?</h3>

                    <p class=" wow fadeInUp" data-wow-delay="0.2s" class="lead" style="margin-top:0">Postúlate</p>

                </div>

                <form role="form" action="" method="post" enctype="multipart/form-data" >

                    <div class="col-md-6 col-lg-offset-3">

                        <div class="form-group">

                            <label>Nombre</label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="nombre" placeholder="Ingrese Nombre" required>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>



                        <div class="form-group">

                            <label >Apellido</label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="apellido" placeholder= "Ingrese Apellido" >

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>



                        <div class="form-group">

                            <label>Cedula</label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="cedula" placeholder= "Ingrese cedula" >

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>

                        <div class="form-group">

                            <label>Teléfono</label>

                            <div class="input-group">

                                <input type="text" class="form-control" name="tlf"  placeholder= "Ingrese telefono">

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>



                        <div class="form-group">

                            <label for="InputEmail">Correo</label>

                            <div class="input-group">

                                <input type="email" class="form-control" name="correo" placeholder="Ingrese correo" required  >

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>



                         <div class="form-group">

                            <label >Enlace de Linkedin</label>

                            <div class="input-group">

                                <input type="url" class="form-control" name="linkedin"  id="linkedin" >

                                <span class="input-group-addon"><i class="fa fa-linkedin form-control-feedback"></i></span>

                            </div>

                        </div>





                         <div class="form-group">

                            <label >Adjuntar Currículum</label>

                            <div class="input-group">

                                <input type="file" class="form-control" name="archivo"  id="archivo" required  >

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>



                        <div class="form-group">

                            <label for="InputMessage">Mensaje</label>

                            <div class="input-group">

                                <textarea name="mensaje" class="form-control" rows="5" required></textarea>

                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>

                            </div>

                        </div>

                        
                     

                         <div class="g-recaptcha" data-sitekey="6LdrHB8UAAAAABPArZEo9oU4ygSD03hi_WwwbIHS" ></div>
                         <br>

                            

                        <input type="submit" name="submit" value="Enviar" class="btn wow tada btn-embossed btn-primary "   />



                        </div>

                                     </form>


                                <?php
                                

                                if (isset($_REQUEST['submit'])) { /* Verifico que se recibio el formulario */

                                        $bHayFicheros = 0;
                                        $sCabeceraTexto = "";
                                        $sAdjuntos = "";
                                        $sCuerpo = $sTexto;
                                        $sSeparador = uniqid("_Separador-de-datos_");
                                        $captcha=$_REQUEST['g-recaptcha-response'];
                                 
                                        $sCabeceras = "MIME-version: 1.0\n";
                                 
                                     
                                        $sCuerpo ="Nombre: " . $_REQUEST['nombre'] . " \r\n" . "Apellido: " . $_REQUEST['apellido'] . " \r\n" . "Cedula: " . $_REQUEST['cedula'] . " \r\n" . "Telefono: " . $_REQUEST['tlf'] . "\r\n" . "Correo: " . $_REQUEST['correo'] . " \r\n" . "Mensaje: " . $_REQUEST['mensaje']." \r\n" . "Linkedin: " . $_REQUEST['linkedin']." \r\n". "Currículum:";
                 
                        
                                      foreach ($_FILES as $vAdjunto)
                                     {
                 
                                    if ($bHayFicheros == 0)
                                     {
                         
                                        $bHayFicheros = 1;
                                        $sCabeceras .= "Content-type: multipart/mixed;";
                                        $sCabeceras .= "boundary=\"".$sSeparador."\"\n";
                                        $sCabeceraTexto = "--".$sSeparador."\n";
                                        $sCabeceraTexto .= "Content-type: text/plain;charset=iso-8859-1\n";
                                        $sCabeceraTexto .= "Content-transfer-encoding: 7BIT\n\n";
                         
                                        $sCuerpo = $sCabeceraTexto.$sCuerpo;
                         
                                    }

                                    if ($vAdjunto["size"] > 0)
                                    {
                                        $sAdjuntos .= "\n\n--".$sSeparador."\n";
                                        $sAdjuntos .= "Content-type: ".$vAdjunto["type"].";name=\"".$vAdjunto["name"]."\"\n";
                                        $sAdjuntos .= "Content-Transfer-Encoding: BASE64\n";
                                        $sAdjuntos .= "Content-disposition: attachment;filename=\"".$vAdjunto["name"]."\"\n\n";
                         
                                        $oFichero = fopen($vAdjunto["tmp_name"], 'rb');
                                        $sContenido = fread($oFichero, filesize($vAdjunto["tmp_name"]));
                                        $sAdjuntos .= chunk_split(base64_encode($sContenido));
                                        fclose($oFichero);
                                    }
                         
                                }
                         
                              
                                if ($bHayFicheros)
                                    $sCuerpo .= $sAdjuntos."\n\n--".$sSeparador."--\n";
                 
                   

                                   $para = 'mpereira@eurogroup.com.ve'; 

                                    $asunto = 'Formulario de postulado';
                                    $sDe = "noreply@eurogroupvenezuela.com";

                                     if ($sDe)$sCabeceras .= "From:".$sDe."\n";
                 
                                     
                                     if($captcha=="")
                                     {
                                        echo "<SCRIPT>alert('Verifique Capcha')</SCRIPT>";
                                        exit;
                                     }
                                     $secret = '6LdrHB8UAAAAAMJwWOznOeoGR6ptO-fda0bIcdET';

                                    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_REQUEST['g-recaptcha-response']);
                                    $responseData = json_decode($verifyResponse);


                                    if($responseData->success){


                                    if (mail($para, $asunto, $sCuerpo, $sCabeceras)) {

                                        echo "<SCRIPT>alert('Mensaje enviado')</SCRIPT>";
                                        
                                    }

                                    }

                                 
                                }

                ?><hr class="featurette-divider hidden-lg">


                <li class="social"> 

                    <a href="http://www.facebook.com/eurogroupca"><i class="fa fa-facebook-square fa-size"> </i></a>

                    <a href="https://www.instagram.com/eurogroup.ca/"><i class="fa  fa-instagram fa-size"> </i> </a> 

                </li>

            </div>

        </div>

    </div>



    <footer>

        <div class="container">

            <div class="row">

                <div class="col-md-7">


             <br>
             <br>
             

                  <a class="footer-title">Create by </a>  <a class="footer-title" target="blank" href="http://oxoftmedia.com/">OXOFTMEDIA </a> <a class="footer-title"> and </a> <a class="footer-title" target="blank" href="http://vccodes.com.ve/"> VC CODES</a>

                </div> <!-- /col-xs-7 -->



            </div>

        </div>

    </footer>



<?php include("script.php"); ?>