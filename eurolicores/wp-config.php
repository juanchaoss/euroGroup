<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eurolicores');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(1I]&UP=;qpr{A sxsk[3{g>kl~uXbfx~~YBEtaGL,-(gE/x M9G:~ *kapA1z4t');
define('SECURE_AUTH_KEY',  '+[9xaf7(4RP-KAOb$)B^  Dn26v5CHy=DMWe7[_0WQeii,/0s,zY$!t=4-_Y&L8N');
define('LOGGED_IN_KEY',    'jY@Ou&1#v8_&5fGinyO`k,8(Wt9XeSU9FV`1Cd^v%)+?1K?c-r=^C=>jh/-OA~Ol');
define('NONCE_KEY',        'In^M.0z?3s3ANV+s>m1w(_VEi!PNb%/%VGdHn~:A.I;Kv XUF iy:kZC(aLTQDY4');
define('AUTH_SALT',        '#s^QeGAj5lA@vH#y|Or/5T,g7B4}5Iz ?>Y(_LlNJ%t|L/:JqI:FWbd8MR,GI7`)');
define('SECURE_AUTH_SALT', ')*CTQwakXb-)n8i1BmA^neP9<$g0d1XZ%$G)%Weo.PI@in~h6AKS-HZnO=0yJ~$M');
define('LOGGED_IN_SALT',   '3^/oVmiQceM-]+x:[M 8]B`qr!L1 ;kr)W-+_)z7l%dTAIkr]MnpK&iSuZiYikm`');
define('NONCE_SALT',       'UQ2KH,Jc8wQZ6C)IwvcVc8J~ W[I%?o6l;-wi!W6DB*-a7$S.~TsJ8yqdk<:`LH6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_eurogroup';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct' );
